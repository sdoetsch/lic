# Line Integral Convolution
Package for visualizing 2D vector fields using the
[line integral convolution](https://en.wikipedia.org/wiki/Line_integral_convolution) method.

This repository provides a packaged version of the [LIC code from the
Scipy Cookbook](https://scipy-cookbook.readthedocs.io/items/LineIntegralConvolution.html)
with a convinience function `convolute`.

## Installation
You can install the package directly with pip:
```
pip install git+https://gitlab.mpcdf.mpg.de/sdoetsch/lic.git
```

Otherwise you can download the repository and run
```
python setup.py install
```
inside the directory.

## Usage examples
Usage examples can be found in the directory `examples`.
