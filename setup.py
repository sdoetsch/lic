from setuptools import setup
from setuptools.extension import Extension

def cythonize(*args, **kwargs):
    from Cython.Build import cythonize
    return cythonize(*args, **kwargs)

def numpy_include():
    import numpy
    return numpy.get_include()

setup(
    name = 'lic',
    version = '0.1',
    packages = ['lic'],
    setup_requires=[
        'setuptools>=18.0',
        'cython',
    ],
    install_requires = [
        'numpy',
        'matplotlib',
        'cython'
    ],
    ext_modules = cythonize(Extension("lic.internal", sources=['lic/internal.pyx'],
                                      include_dirs=[numpy_include()], extra_compile_args=['-g0']))
)
